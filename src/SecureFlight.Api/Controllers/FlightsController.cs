﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController : SecureFlightBaseController
{
    private readonly IService<Flight> _flightService;
    private readonly IService<PassengerFlight> _passengerFlightService;

    public FlightsController(IService<Flight> flightService, IService<PassengerFlight> passengerFlightService, IMapper mapper)
        : base(mapper)
    {
        _flightService = flightService;
        _passengerFlightService = passengerFlightService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await _flightService.GetAllAsync();
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet]
    [Route("GetByOriginAndDestination")]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetByOriginAndDestination(string originAirport, string destinationAirport)
    {
        var flights = await _flightService.FilterAsync(x => x.OriginAirport == originAirport && x.DestinationAirport == destinationAirport);
        return GetResult<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPost]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public IActionResult Add(string passengerId, long flightId)
    {
        var passengerFlight = new PassengerFlight()
        {
            FlightId = flightId,
            PassengerId = passengerId,
        };

        return Ok(_passengerFlightService.Add(passengerFlight));
    }

}